;;; company-coq-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "company-coq" "company-coq.el" (21938 34283
;;;;;;  303446 422000))
;;; Generated autoloads from company-coq.el

(autoload 'company-coq-tutorial "company-coq" "\


\(fn)" t nil)

(autoload 'company-coq-initialize "company-coq" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("company-coq-abbrev.el" "company-coq-pkg.el"
;;;;;;  "company-coq-tg.el") (21938 34347 556517 381000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; company-coq-autoloads.el ends here
