        
; @begin(85644648)@ - Do not edit these lines - added automatically!
(if (file-exists-p "/home/yagox/git/ciao-devel/ide/emacs-mode/ciao-site-file.el")
(load-file "/home/yagox/git/ciao-devel/ide/emacs-mode/ciao-site-file.el")
)
; @end(85644648)@ - End of automatically added lines.

(require 'package)

(add-to-list 'package-archives
         '("melpa" . "http://melpa.milkbox.net/packages/"))
(package-initialize)

(load "~/.emacs.d/elixir-settings.el")

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; (load-theme 'zenburn t)
;;(load-theme 'solarized-dark t)
;; (require 'moe-theme)
;; (moe-dark)

(load-theme 'zonokai-blue t)

;;(load-theme 'moe-dark t)
(column-number-mode t)
(setq-default c-basic-offset 4)
(setq-default indent-tabs-mode nil)
;; (powerline-default-theme)
(setq inhibit-startup-screen t)

;; Cambia alt a cmd
(setq mac-option-modifier nil
      mac-command-modifier 'meta
      x-select-enable-clipboard t)

;; Visible bell
(setq visible-bell 1)
(setq ring-bell-function 'ignore)

;; magit-status
(global-set-key (kbd "M-m") 'magit-status)

;; ace-jump-buffer
(global-set-key (kbd "C-<return>") 'ace-jump-mode)

;; Mac Path
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

;; powerline
(powerline-default-theme)

;; highlight
(global-set-key (kbd "C-f") 'highlight-symbol-at-point)
(global-set-key (kbd "C-n") 'highlight-symbol-next)
(global-set-key (kbd "C-;") 'highlight-symbol-prev)
;; (global-set-key [(meta f3)] 'highlight-symbol-query-replace)

;; Alias yes or no
(defalias 'yes-or-no-p 'y-or-n-p)

;; windmove
(global-set-key (kbd "M-C-<left>")  'windmove-left)
(global-set-key (kbd "M-C-<right>") 'windmove-right)
(global-set-key (kbd "M-C-<up>")    'windmove-up)
(global-set-key (kbd "M-C-<down>")  'windmove-down)

;; Anzu mode (highliht on search)
(global-anzu-mode +1)
(global-set-key (kbd "M-%") 'anzu-query-replace)
(global-set-key (kbd "C-M-%") 'anzu-query-replace-regexp)

;; Indent guide
(indent-guide-global-mode)

;; auto-complete
(auto-complete-mode 1)

;; Irony-mode C/C++
;; (add-hook 'c++-mode-hook 'irony-mode)
;; (add-hook 'c-mode-hook 'irony-mode)
;; (add-hook 'objc-mode-hook 'irony-mode)

;; replace the `completion-at-point' and `complete-symbol' bindings in
;; irony-mode's buffers by irony-mode's function
(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

;; Visual Regexp
(define-key global-map (kbd "C-c r") 'vr/replace)
(define-key global-map (kbd "C-c q") 'vr/query-replace)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(paradox-github-token t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Save session
;; (desktop-save-mode 1)

;; resize buffers
(global-set-key (kbd "M-S-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "M-S-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "M-S-<down>") 'shrink-window)
(global-set-key (kbd "M-S-<up>") 'enlarge-window)

;; smartparents-mode
(require 'smartparens-config)
(smartparens-global-mode)
;; smex
(global-set-key (kbd "M-x") 'smex)

;; Projectile
;; (projectile-global-mode)
;; Rainbown delimiter mode
(rainbow-delimiters-mode)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(add-hook 'ciao-mode #'rainbow-delimiters-mode)

;; Ensime for scala-mode
(require 'ensime)
(add-hook 'scala-mode-hook 'ensime-scala-mode-hook)

;; Tuareg for F*
(add-hook 'tuareg-mode-hook 'tuareg-imenu-set-imenu)
(setq auto-mode-alist
      (append '(("\\.ml[ily]?$" . tuareg-mode)
                ("\\.topml$" . tuareg-mode))
              auto-mode-alist))
(setq auto-mode-alist 
      (append '(("\\.fs[tiy]?$" . tuareg-mode))
              auto-mode-alist))

(add-to-list 'compilation-error-regexp-alist
             '("\\([0-9a-zA-Z_-]*.fst\\)(\\([0-9]+\\)\\,\\([0-9]+\\)-[0-9]+\\,[0-9]+)" 1 2 3))
(add-to-list 'compilation-error-regexp-alist
             '("^ERROR: Syntax error near line \\(.[0-9]+\\), character \\(.[0-9]+\\) in file \\(.*\\)$" 3 1 2))

;; Change selected color
(set-face-attribute 'region nil :background "#868A08")

;; Font Size
(set-face-attribute 'default nil :height 105)
